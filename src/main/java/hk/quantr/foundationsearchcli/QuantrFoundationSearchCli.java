package hk.quantr.foundationsearchcli;

import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.PropertyUtil;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrFoundationSearchCli {

    public static final Logger logger = Logger.getLogger(QuantrFoundationSearchCli.class.getName());

    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("v", "version", false, "display version");
        options.addOption("i", "index", true, "index folder path, for search only");
        options.addOption("o", "output", true, "output format, for search only");
        options.addOption("q", "token", true, "search query");

        if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar gitlab-lucene-indexer-xx.jar [OPTION] <input file>", options);
            return;
        }
        if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
            System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

            TimeZone utc = TimeZone.getTimeZone("UTC");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            format.setTimeZone(utc);
            Calendar cl = Calendar.getInstance();
            try {
                Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
                cl.setTime(convertedDate);
                cl.add(Calendar.HOUR, 8);
            } catch (java.text.ParseException ex) {
                logger.log(Level.INFO, ex.getMessage(), ex);
            }
            System.out.println("build date : " + format.format(cl.getTime()) + " HKT");
            return;
        }

        new QuantrFoundationSearchCli().start(options, args);
    }

    private void start(Options options, String[] args) {
        try {
            CommandLineParser cliParser = new DefaultParser();
            CommandLine cmd = cliParser.parse(options, args);
            String q = cmd.getOptionValue("q");
            if (q == null) {
                System.out.println("please provide query by -q");
                return;
            }

            String myQuery = q;
            if (!q.startsWith("content:") && !q.contains(":")) {
                myQuery = "content:" + myQuery;
            }

            Analyzer analyzer = new StandardAnalyzer();

            QueryParser queryParser = new QueryParser("MyParser", analyzer);
//					Query query = queryParser.parse("content:peter@quantr.hk");
            Query query = queryParser.parse(myQuery);

            File indexFolder;
            if (cmd.hasOption("i")) {
                indexFolder = new File(cmd.getOptionValue("i"));
            } else {
                indexFolder = new File("index");
            }
            if (!indexFolder.exists()) {
                System.out.println("index folder not exist : " + cmd.getOptionValue("i"));
                System.exit(1);
            }
            Directory directory = FSDirectory.open(indexFolder.toPath());
            IndexReader indexReader = DirectoryReader.open(directory);
            IndexSearcher searcher = new IndexSearcher(indexReader);
            TopDocs topDocs = searcher.search(query, 10);
            if (!cmd.hasOption("o") || !cmd.getOptionValue("o").equals("json")) {
                System.out.println("Search result: " + topDocs.totalHits);
                System.out.println("-".repeat(100));
            }
            ScoreDoc[] scoreDocs = topDocs.scoreDocs;
            JSONArray arr = new JSONArray();
            for (ScoreDoc scoreDoc : scoreDocs) {
                int docId = scoreDoc.doc;
//                float score = scoreDoc.score;
                Document doc = searcher.doc(docId);

                if (cmd.hasOption("o") && cmd.getOptionValue("o").equals("json")) {
                    JSONObject obj = new JSONObject();
                    for (IndexableField field : doc.getFields()) {
                        obj.put(field.name(), field.stringValue());
                    }
//                    obj.put("category", doc.get("category"));
//                    obj.put("docID", docId);
//                    obj.put("score", score);
//                    obj.put("projectName", doc.get("projectName"));
//                    obj.put("web_url", doc.get("web_url"));
//                    obj.put("branchName", doc.get("branchName"));
//                    obj.put("avatar_url", doc.get("avatar_url"));
//                    obj.put("gitUrl", doc.get("gitUrl"));
//                    obj.put("filename", doc.get("filename"));
//                    obj.put("filepath", doc.get("filepath"));
//                    obj.put("content", doc.get("content"));
//
                    JSONArray linesObj = new JSONArray();
                    String content = doc.get("content");
                    String lines[] = content.split("\r?\n");
                    int lineNo = 0;
                    for (String line : lines) {
                        if (line.toLowerCase().contains(q.toLowerCase())) {
                            JSONObject temp = new JSONObject();
                            temp.put("lineNo", lineNo);
                            temp.put("line", line);
                            linesObj.put(temp);
                        }
                        lineNo++;
                    }
                    obj.put("lines", linesObj);
                    arr.put(obj);
                } else {
                    for (IndexableField field : doc.getFields()) {
                        System.out.println(field.name() + " = " + field.stringValue());
                    }
//                    System.out.println("docID= " + docId + " , score= " + score);
//                    System.out.println("projectName: " + doc.get("projectName"));
//                    System.out.println("gitUrl: " + doc.get("gitUrl"));
//                    System.out.println("web_url: " + doc.get("web_url"));
//                    System.out.println("branchName: " + doc.get("branchName"));
//                    System.out.println("avatar_url: " + doc.get("avatar_url"));
//                    System.out.println("filename: " + doc.get("filename"));
//                    System.out.println("filepath: " + doc.get("filepath"));
//                    String content = doc.get("content");
//                    String lines[] = content.split("\r?\n");
//                    int lineNo = 0;
//                    for (String line : lines) {
//                        if (line.toLowerCase().contains(q.toLowerCase())) {
//                            System.out.println(lineNo + ": " + line);
//                        }
//                        lineNo++;
//                    }
//                    System.out.println("-".repeat(100));
                }
            }

            if (cmd.hasOption("o") && cmd.getOptionValue("o").equals("json")) {
                System.out.println(CommonLib.prettyFormatJson(arr.toString()));
            }
            indexReader.close();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(QuantrFoundationSearchCli.class.getName()).log(Level.SEVERE, null, ex);
        } catch (org.apache.lucene.queryparser.classic.ParseException ex) {
            Logger.getLogger(QuantrFoundationSearchCli.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
